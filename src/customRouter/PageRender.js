import React from "react";
import { useParams } from "react-router-dom";
import NotFound from "../components/NotFound";
import { useSelector } from "react-redux";
import { selectUser } from "../features/authSlice";

const generatePage = (pageName) => {
  const component = () => require(`../components/${pageName}`).default;
  console.log(pageName);
  try {
    return React.createElement(component());
  } catch (err) {
      console.log(err)
    return <NotFound />;
  }
};

const PageRender = () => {
  const { page, id } = useParams();
  const user = useSelector(selectUser);
  let pageName = "";
  if (user.token) {
    if (id) {
      pageName = `${page}/[id]`;
    } else {
      pageName = `${page}`;
    }
  }

  return generatePage(pageName);
};

export default PageRender;
