import { Route, Redirect } from "react-router-dom";
import { selectUser } from "../features/authSlice";
import { useSelector } from "react-redux";

const PrivateRouter = ({ component: Component, ...rest }) => {
  const user = useSelector(selectUser);
  return (
    <Route
      {...rest}
      component={(props) => {
        if (user.token) {
          return <Component {...props} />;
        } else {
          return <Redirect to={`/`} />;
        }
      }}
    />
  );
};

export default PrivateRouter;
