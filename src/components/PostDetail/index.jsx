import { Button, Typography } from "antd";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import * as timeago from "timeago.js";
import { Input } from "antd";
import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import {
  BiWinkSmile,
  BiHeart,
  BiPaperPlane,
  BiComment,
  BiBookmark,
} from "react-icons/bi";
import axiosClient from "../../axios/aixos";
import { Spin } from "antd";
import styles from "./PostDetail.module.scss";

const { Title } = Typography;
const imagesLink = "http://localhost:8800/images/";
const { TextArea } = Input;

export default function PostDetail() {
  const [unMounted, setUnmouted] = useState(true);
  const [infoPost, setInfoPost] = useState({});
  const [cmtOfPosts, setCmtOfPost] = useState([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [pickerOpen, setPickerOpen] = useState(false);
  const idPost = useParams().id;
  const PF = "http://localhost:8800/images/person/noAvatar.png";
  const fetchInfoPost = async () => {
    const rs = await axiosClient.get(`/api/post/${idPost}`);
    setInfoPost(rs.data);
    setLoading(false);
  };
  const getComments = async () => {
    const rs = await axiosClient.get(
      `/api/comment/getCommentOfPost??limit=9&page=${page}&postId=${idPost}`
    );

    setCmtOfPost(rs.data.cmts);
    setCmtOfPost(cmtOfPosts.concat(rs.data.cmts));
    setPage(page + 1);
  };
  useEffect(() => {
    setUnmouted(true);
    fetchInfoPost();
    getComments();
    return () => setUnmouted(false);
  }, []);

  if (!unMounted) {
    return null;
  }

  return (
    <div className={styles.containerPostDetail}>
      {loading ? (
        <Spin />
      ) : (
        <div className={styles.containerPost}>
          <div className={styles.articleContainer}>
            <div className={styles.imgPost}>
              <img src={imagesLink + infoPost.img} alt="" />
            </div>
            <div className={styles.header}>
              <div className={styles.containerUserAndAvt}>
                <div className={styles.avtImg}>
                  <img
                    src={
                      infoPost?.userId.profilePicture
                        ? PF + infoPost?.userId.profilePicture
                        : PF
                    }
                    alt=""
                  />
                </div>
                <p>{infoPost?.userId.userName}</p>
              </div>

              <p className={styles.timeAgo}>
                {timeago.format(infoPost?.createdAt)}
              </p>
            </div>
            <div className={styles.containerCmtDetail}>
              <div className={styles.containerCmt}>
                <div className={styles.cmt}>
                  <div className={styles.containerUserAndAvt}>
                    <div className={styles.avtImg}>
                      <img
                        src={
                          infoPost?.userId.profilePicture
                            ? PF + infoPost?.userId.profilePicture
                            : PF
                        }
                        alt=""
                      />
                    </div>
                    <p>{infoPost?.userId.userName}</p>
                  </div>
                  <p>{infoPost?.desc}</p>
                </div>
                {cmtOfPosts?.map((cmt) => (
                  <div key={cmt._id} className={styles.cmt}>
                    <div className={styles.containerUserAndAvt}>
                      <div className={styles.avtImg}>
                        <img
                          src={
                            cmt?.userId.profilePicture
                              ? PF + cmt?.userId.profilePicture
                              : PF
                          }
                          alt=""
                        />
                      </div>
                      <p>{cmt?.userId.userName}</p>
                    </div>
                    <p className={styles.cmtContent}>{cmt?.content}</p>
                    <p className={styles.timeAgo}>{timeago.format(cmt.createdAt)}</p>
                  </div>
                ))}
              </div>
              <div>
                <div className={styles.toolbar}>
                  <div className={styles.containerToolbar}>
                    <div className={styles.toolbarFunction}>
                      <p className={styles.like}>
                        <BiHeart />
                      </p>
                      <p className={styles.comment}>
                        <BiComment />
                      </p>
                      <p className={styles.transfer}>
                        <BiPaperPlane />
                      </p>
                    </div>
                    <p className={styles.psignet}>
                      <BiBookmark />
                    </p>
                  </div>
                </div>
                <div className={styles.containLiked}>
                  <p className={styles.amountLike}>
                    {infoPost?.likes.length} people
                  </p>
                  <p>liked</p>
                </div>
              </div>
              <div className={styles.sectionCmt}>
                <form
                  className={styles.formComment}
                  action=""
                  onSubmit={() => {}}
                >
                  <div className={styles.containInput}>
                    <div className={styles.containerIcon}>
                      {pickerOpen && (
                        <Picker
                          showPreview="false"
                          skin={2}
                          defaultSkin={5}
                          onClick={() => {}}
                          style={{
                            position: "absolute",
                            top: "-28rem",
                            right: "-17.44rem",
                          }}
                        />
                      )}
                      <div className={styles.btnEmoji} onClick={() => {}}>
                        <BiWinkSmile />
                      </div>
                    </div>

                    <div className={styles.inputStatus}>
                      <TextArea
                        className={styles.inputArea}
                        // value={comment}
                        onClick={() => {}}
                        onChange={(e) => e}
                        // ref={""}
                        bordered={false}
                        placeholder="Your comment here"
                        autoSize={{ minRows: 1, maxRows: 8 }}
                      />
                    </div>
                    <div>
                      <button
                        type="submit"
                        // disabled={!comment}
                        className={styles.buttonPost}
                      >
                        Post
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
