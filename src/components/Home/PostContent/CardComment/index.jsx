import { useEffect, useState } from "react";
import axiosClient from "../../../../axios/aixos";
import style from "./Card.module.scss";
export default function CardComment(props) {
  const { infoCmtPost } = props;
  const [ownCmt, setOwnCmt] = useState({});
  const fetchOwnCmt = async () => {
    const rs = await axiosClient.get(
      `/api/user/get/user?userId=${
        infoCmtPost?.comments[infoCmtPost?.comments.length - 1]?.userId
      }`
    );
    setOwnCmt(rs.data);
  };
  useEffect(() => {
    if (
      typeof infoCmtPost?.comments[infoCmtPost?.comments.length - 1]?.userId ===
      "string"
    ) {
      fetchOwnCmt();
    }
  }, [infoCmtPost]);
  return (
    <div>
      <p className={style.amountCmt}>
        View all {infoCmtPost?.comments.length} comments
      </p>
      <p className={style.lastCmt}>
        <strong>
          {
            infoCmtPost?.comments[infoCmtPost?.comments.length - 1]?.userId
              .userName || ownCmt.userName
          }
          &nbsp;&nbsp;
        </strong>
        {infoCmtPost?.comments[infoCmtPost?.comments.length - 1]?.content}
      </p>
    </div>
  );
}
