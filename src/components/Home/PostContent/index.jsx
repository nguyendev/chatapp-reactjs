import { useEffect, useState } from "react";
import { IoIosCheckmarkCircle } from "react-icons/io";
import { BsFillHeartFill } from "react-icons/bs";
import { useSelector } from "react-redux";
import axiosClient from "../../../axios/aixos";
import { BsFillBookmarkFill } from "react-icons/bs";
import styles from "./Post.module.scss";
import * as timeago from "timeago.js";
import {
  BiWinkSmile,
  BiHeart,
  BiPaperPlane,
  BiComment,
  BiBookmark,
} from "react-icons/bi";
import { selectUser } from "../../../features/authSlice";
import { Input } from "antd";
import CardComment from "./CardComment";
import "emoji-mart/css/emoji-mart.css";
import io from "socket.io-client";
import { Picker } from "emoji-mart";
import "./Post.module.scss";
import { Link } from "react-router-dom";

const socket = io("ws://localhost:8800/", { transports: ["websocket"] });

const { TextArea } = Input;
export default function Post(props) {
  const { PF, post } = props;
  const [pickerOpen, setPickerOpen] = useState(false);
  const [isSave, setIsSave] = useState(false);
  const [comment, setComment] = useState("");
  const [cmtSocket, setCmtSocket] = useState(null);
  const [likeAnimation, setIsLikeAnimation] = useState(false);
  const user = useSelector(selectUser);

  const imagesLink = "http://localhost:8800/images/";

  const onClickIcon = (emoji) => {
    setComment((prevContent) => (prevContent += emoji.native));
  };

  const handleSubmitCmt = async (e) => {
    e.preventDefault();
    const newComment = {
      content: comment,
      userId: user._id,
      postId: post._id,
      postUserId: post.userId._id,
    };
    const newPost = { ...post, comments: [...post.comments, newComment] };
    if (comment.trim() !== "") {
      await axiosClient.post("/api/comment/createComment", newComment);
      setComment("");
      socket.emit("createComment", newPost);
    }
  };

  useEffect(() => {
    socket.on(`${post.userId._id}`, (newPost) => {
      if (newPost._id === post._id) {
        setCmtSocket(newPost);
      }
    });

    return () => socket.off(`${post.userId._id}`);
  }, [socket]);

  useEffect(() => {
    if (user?.saved?.includes(post?._id)) {
      setIsSave(true);
    }
    setCmtSocket(post);
  }, []);

  const handleTogglePicker = () => {
    setPickerOpen(!pickerOpen);
  };

  const handleOnchangeComment = (e) => {
    setComment(e.target.value);
  };
  const handleLike = async () => {
    await axiosClient.put(`/api/post/${post._id}/like`, { userId: user._id });
    if (cmtSocket?.likes?.includes(user._id)) {
      const likes = cmtSocket?.likes.filter((item) => item !== user._id);
      socket.emit("likePost", { ...post, likes });
    } else {
      socket.emit("likePost", {
        ...post,
        likes: [...cmtSocket?.likes, user._id],
      });
    }
    setIsLikeAnimation(true);
    setTimeout(function () {
      setIsLikeAnimation(false);
    }, 1000);
  };

  const handleSave = async () => {
    await axiosClient.put(`/api/post/${post._id}/save?_id=${user._id}`);
    setIsSave(!isSave);
  };
  return (
    <>
      <div className={styles.postItem}>
        <div className={styles.inforUserPost}>
          <div className={styles.avtUser}>
            <div className={styles.avtImg}>
              <img
                src={
                  post.userId.profilePicture
                    ? PF + post.userId.profilePicture
                    : PF
                }
                alt=""
              />
            </div>
          </div>
          <div className={styles.userName}>
            <h3>{post.userId.userName}</h3>
            <span>
              <IoIosCheckmarkCircle className={styles.iconCheck} />
            </span>
          </div>
        </div>
        <div>
          <div className={styles.imgContainer} onDoubleClick={handleLike}>
            <img src={`${imagesLink}/${post.img}`} alt="Publication Photo" />
            <div className={styles.doubleClickLiked}>
              {likeAnimation && (
                <BsFillHeartFill className={styles.iconHeartBeat} />
              )}
            </div>
          </div>
          <div className={styles.toolbar}>
            <div className={styles.toolbarFunction}>
              <p className={styles.like} onClick={handleLike}>
                {cmtSocket?.likes.includes(user._id) ? (
                  <BsFillHeartFill />
                ) : (
                  <BiHeart />
                )}
              </p>
              <Link className={styles.link} to={`/post/${post._id}`}>
                <p className={styles.comment}>
                  <BiComment />
                </p>
              </Link>

              <p className={styles.transfer}>
                <BiPaperPlane />
              </p>
            </div>
            <p onClick={handleSave} className={styles.psignet}>
              {isSave ? <BsFillBookmarkFill /> : <BiBookmark />}
            </p>
          </div>
          <div className={styles.description}>
            <a href="#" className={styles.amountLike}>
              {cmtSocket?.likes?.length} like
            </a>
          </div>
          <div className={styles.description}>
            <strong>{post.userId.userName}&nbsp;&nbsp;</strong>
            {post.desc}
          </div>
          <div className={styles.description}>
            <CardComment infoCmtPost={cmtSocket} />
          </div>
          <div className={styles.description}>
            <span className={styles.postDate}>
              {timeago.format(post.createdAt)}
            </span>
          </div>
          <form
            className={styles.formComment}
            action=""
            onSubmit={handleSubmitCmt}
          >
            <div className={styles.containInput}>
              <div className={styles.containerIcon}>
                {pickerOpen && (
                  <Picker
                    showPreview="false"
                    skin={2}
                    defaultSkin={5}
                    onClick={(emoji) => onClickIcon(emoji)}
                    style={{
                      position: "absolute",
                      top: "-28rem",
                      right: "-17.44rem",
                    }}
                  />
                )}
                <div className={styles.btnEmoji} onClick={handleTogglePicker}>
                  <BiWinkSmile />
                </div>
              </div>

              <div className={styles.inputStatus}>
                <TextArea
                  className={styles.inputArea}
                  value={comment}
                  onClick={() => {}}
                  onChange={(e) => handleOnchangeComment(e)}
                  // ref={""}
                  bordered={false}
                  placeholder="Your comment here"
                  autoSize={{ minRows: 1, maxRows: 8 }}
                />
              </div>
              <div>
                <button
                  type="submit"
                  disabled={!comment}
                  className={styles.buttonPost}
                >
                  Post
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
