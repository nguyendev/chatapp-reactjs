import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Modal from "../../packages/tungtt-modal";
import UploadModal from "../../components/UploadModal";
import { selectUser } from "../../features/authSlice";
import { Spin } from "antd";
import styles from "./Home.module.scss";
import Post from "./PostContent";
import axiosClient from "../../axios/aixos";
import InfiniteScroll from "react-infinite-scroll-component";
import io from "socket.io-client";

const socket = io("ws://localhost:8800/", { transports: ["websocket"] });
const UPLOAD_MODAL = "UPLOAD_MODAL";

export default function Home() {
  const [unMounted, setUnmouted] = useState(false);
  const [modal, setModal] = useState(null);
  const [posts, setPosts] = useState([]);
  const [page, setPage] = useState(1);
  const [postGetCurrent, setGetPostCurrent] = useState([]);
  const user = useSelector(selectUser);
  const PF = "http://localhost:8800/images/person/noAvatar.png";

  const handleClickUpload = () => {
    setModal(UPLOAD_MODAL);
  };

  useEffect(() => {
    socket.emit("joinUser", user);
  }, [user]);

  const handleCloseModal = () => {
    setModal(null);
  };

  const fetchMoreData = async () => {
    const rs = await axiosClient.get(
      `http://localhost:8800/api/post/get/post/on/feed?limit=3&page=${page}&userId=${user._id}`
    );
    setGetPostCurrent(rs.data.posts);
    setPosts(posts.concat(rs.data.posts));
    setPage(page + 1);
  };

  useEffect(() => {
    setUnmouted(true);
    fetchMoreData();
    return () => setUnmouted(false);
  }, []);

  if (!unMounted) {
    return null;
  }

  return (
    <>
      <div className={styles.containerHome}>
        <div className={styles.wrapperHome}>
          <div className={styles.wrapperHomeLeft}>
            <div className={styles.storyFriend}></div>
            <div className={styles.uploadPost}>
              <div className={styles.uploadLayoutItem}>
                <div className={styles.imgUser}>
                  <img
                    src={user.profilePicture ? PF + user.profilePicture : PF}
                    alt=""
                  />
                </div>
                <div
                  className={styles.openInputStatus}
                  onClick={handleClickUpload}
                >
                  Hi, what are you thinking?
                </div>
              </div>
              <div className={styles.uploadLayoutItem}></div>
            </div>
            <div className={styles.listPost}>
              <InfiniteScroll
                dataLength={posts.length}
                next={fetchMoreData}
                hasMore={true}
                loader={
                  postGetCurrent.length || posts.length ? (
                    ""
                  ) : (
                    <Spin size="large" />
                  )
                }
              >
                {posts.map((post) => (
                  <Post key={post._id} PF={PF} post={post} />
                ))}
              </InfiniteScroll>
            </div>
          </div>
          <div className={styles.wrapperHomeRight}></div>
        </div>
      </div>

      <Modal
        isOpen={modal === UPLOAD_MODAL}
        noPadding
        onRequestClose={handleCloseModal}
      >
        <UploadModal onClose={handleCloseModal} />
      </Modal>
    </>
  );
}
