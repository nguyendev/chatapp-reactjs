import styles from "./PopoverMenu.module.scss";
import { ImUser, ImCog, ImBookmark, ImTab } from "react-icons/im";
import { Link } from "react-router-dom";

export default function PopoverMenu(props) {
  const {user} = props
  return (
    <div className={styles.popAvatar}>
      <Link to={`/profile/${user._id}`}>
        <div className={styles.popItem}>
          <ImUser className={styles.iconItem} />
          <p className={styles.contentPopMenuItem}> Trang cá nhân</p>
        </div>
      </Link>

      <div className={styles.popItem}>
        <ImBookmark className={styles.iconItem} />
        <p className={styles.contentPopMenuItem}> Đã lưu</p>
      </div>
      <div className={styles.popItem}>
        <ImCog className={styles.iconItem} />
        <p className={styles.contentPopMenuItem}> Cài đặt</p>
      </div>
      <div className={styles.popItem}>
        <ImTab className={styles.iconItem} />
        <p className={styles.contentPopMenuItem}> Chuyển đổi tài khoản</p>
      </div>
    </div>
  );
}
