import { useState } from "react";
import { useSelector } from "react-redux";
import {
  AiFillHome,
  AiOutlineHome,
  AiOutlineHeart,
  AiFillHeart,
  AiOutlineCompass,
  AiFillCompass,
} from "react-icons/ai";
import { RiMessengerLine, RiMessengerFill } from "react-icons/ri";
import { Popconfirm } from "antd";
import styles from "./Navigation.module.scss";
import PopoverMenu from "./PopoverMenu/PopoverMenu";
import { useDispatch } from "react-redux";
import Insta from "../../assets/img/Insta.png";
import { authLogout } from "../../features/authSlice";
import { selectUser } from "../../features/authSlice";
import DebounceSelect from "../Navigation/DebounceSelect";
import axiosClient from "../../axios/aixos";
import { Link } from "react-router-dom";

const Navigation = (props) => {
  const dispatch = useDispatch();
  const [navActive, setNavActive] = useState(0);
  const [value, setValue] = useState([]);

  const logOut = () => {
    dispatch(authLogout());
  };

  const fetchUserList = async (username) => {
    let listUser = [];
    listUser = await axiosClient.get(
      `http://localhost:8800/api/user/find/user?userName=${username}`
    );
    let rs = listUser.data.map((user) => ({
      label: user.userName,
      img: user.profilePicture,
      value: user._id,
    }));
    return rs;
  };

  const user = useSelector(selectUser);
  const PF = "http://localhost:8800/images/person/noAvatar.png";
  // const isAuthenticated = useSelector(selectIsAuthenticated);
  return (
    <div className={styles.containerHeader}>
      <div className={styles.wrapperHeader}>
        <div className={styles.socicalLogo}>
          <img src={Insta} alt="" />
        </div>
        <div className={styles.searchBox}>
          <DebounceSelect
            mode="multiple"
            value={value}
            placeholder="Select users"
            fetchOptions={fetchUserList}
            onChange={(newValue) => {
              setValue(newValue);
            }}
            style={{
              width: "100%",
            }}
          />
        </div>

        <div className={styles.headerFunction}>
          <Link to="/">
            <div
              className={styles.headerFunctionItem}
              onClick={() => setNavActive(0)}
            >
              {navActive === 0 ? (
                <AiFillHome
                  className={[styles.iconFunct, styles.active].join(" ")}
                />
              ) : (
                <AiOutlineHome className={styles.iconFunct} />
              )}
            </div>
          </Link>

          <div
            className={styles.headerFunctionItem}
            onClick={() => setNavActive(1)}
          >
            {navActive === 1 ? (
              <RiMessengerFill
                className={[styles.iconFunct, styles.active].join(" ")}
              />
            ) : (
              <RiMessengerLine className={styles.iconFunct} />
            )}
          </div>
          <div
            className={styles.headerFunctionItem}
            onClick={() => setNavActive(2)}
          >
            {navActive === 2 ? (
              <AiFillCompass
                className={[styles.iconFunct, styles.active].join(" ")}
              />
            ) : (
              <AiOutlineCompass className={styles.iconFunct} />
            )}
          </div>
          <div
            className={styles.headerFunctionItem}
            onClick={() => setNavActive(3)}
          >
            {navActive === 3 ? (
              <AiFillHeart
                className={[styles.iconFunct, styles.active].join(" ")}
              />
            ) : (
              <AiOutlineHeart className={styles.iconFunct} />
            )}
          </div>

          <Popconfirm
            className="prop-test"
            placement="bottom"
            icon=""
            title={<PopoverMenu user={user} />}
            onConfirm={logOut}
            okText="Logout"
            cancelText="Cancel"
          >
            <div
              className={[styles.headerFunctionItem, styles.avtUser].join(" ")}
              onClick={() => setNavActive(4)}
            >
              <img
                src={user.profilePicture ? PF + user.profilePicture : PF}
                alt=""
              />
            </div>
          </Popconfirm>
        </div>
      </div>
    </div>
  );
};

export default Navigation;
