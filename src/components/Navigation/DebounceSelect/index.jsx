import { useState, useRef, useMemo } from "react";
import { Select, Spin } from "antd";
import debounce from "lodash/debounce";
import { Link } from "react-router-dom";
import styles from "./DebounceSelect.module.scss"
const { Option } = Select;
const PF = "http://localhost:8800/images/person/noAvatar.png";

function DebounceSelect({ fetchOptions, debounceTimeout = 800, ...props }) {
  const [fetching, setFetching] = useState(false);
  const [options, setOptions] = useState([]);
  const fetchRef = useRef(0);
  const debounceFetcher = useMemo(() => {
    const loadOptions = (value) => {
      fetchRef.current += 1;
      const fetchId = fetchRef.current;
      setOptions([]);
      setFetching(true);
      fetchOptions(value).then((newOptions) => {
        if (fetchId !== fetchRef.current) {
          // for fetch callback order
          return;
        }

        setOptions(newOptions);
        setFetching(false);
      });
    };

    return debounce(loadOptions, debounceTimeout);
  }, [fetchOptions, debounceTimeout]);

  return (
    <Select
      mode="tags"
      labelInValue
      filterOption={false}
      onSearch={debounceFetcher}
      notFoundContent={fetching ? <Spin size="small" /> : null}
      {...props}
    >
      {options.map((element) => (
        <Option key={element.value} value={element.value}>
          <Link className={styles.linkUser} to={`/profile/${element.value}`}>
            <div>
              <p>{element?.label}</p>
            </div>
          </Link>
        </Option>
      ))}
    </Select>
  );
} // Usage of DebounceSelect

export default DebounceSelect;
