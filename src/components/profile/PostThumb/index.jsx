import styles from "./PostThumb.module.scss";
import { BiHeart, BiComment } from "react-icons/bi";

const imagesLink = "http://localhost:8800/images/";

export default function PostThumb(props) {
  const { posts } = props;
  return (
    <div className={styles.post_thumb}>
      {posts.map((post) => (
        <div key={post._id} className={styles.post_thumb_display}>
          <img src={imagesLink + post.img} style={{ filter: "invert(0)" }} />

          <div className={styles.post_thumb_menu}>
            <div className={styles.icon}>
              <BiHeart />
              <p>{post.likes.length}</p>
            </div>
            <div className={styles.icon}>
              <BiComment />
              <p>{post.comments.length}</p>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
