import styles from "./Info.module.scss";
import Avatar from "./Avatar";
import { Button, Typography } from "antd";
import { selectUser } from "../../../features/authSlice";
import { useSelector } from "react-redux";

const { Title } = Typography;
const PF = "http://localhost:8800/images/person/noAvatar.png";
export default function Info(props) {
  const { profile } = props;
  const currentUser = useSelector(selectUser);

  return (
    <div className={styles.info}>
      <div className={styles.info_container}>
        <Avatar
          src={profile?.picture ? profile.picture : PF}
          size="supper-avatar"
        />
        <div className={styles.info_content}>
          <div className={styles.info_content_title}>
            <h2>{profile?.userName}</h2>
            {profile?._id === currentUser?._id ? (
              <Button className={styles.editProfileBtn}>Edit profile</Button>
            ) : (
              <div>
                <Button className={styles.editProfileBtn}>{profile?.followers.includes(currentUser?._id) ? "Unfollow" : "Follow user"}</Button>
                <Button className={styles.editProfileBtn}>Send messages</Button>
              </div>
            )}
          </div>
          <div className={styles.follow_btn}>
            <span className="mr-4" onClick={() => {}}>
              <strong>22</strong> Articles
            </span>
            <span className="mr-4" onClick={() => {}}>
              <strong> {profile?.followers.length}</strong> Followers
            </span>
            <span className="ml-4" onClick={() => {}}>
              <strong> {profile?.followings.length}</strong> Following
            </span>
          </div>
          <Title level={5}>
            Nguyen.X.Nguyen <span className="text-danger">123456789</span>
          </Title>
          <p className="m-0">Ha Noi</p>
          <Title level={5}>{profile?.email}</Title>
          <a href="#" target="_blank" rel="noreferrer">
            facebook.com.vn
          </a>
          <p>Hello I'm a developer</p>
        </div>
      </div>
    </div>
  );
}
