import { useEffect, useState } from "react";
import styles from "./Profile.module.scss";
import Info from "./Info";
import { Spin } from "antd";
import { useParams } from "react-router-dom";
import axiosClient from "../../axios/aixos";
import PostThumb from "./PostThumb";

const PF = "http://localhost:8800/images/person/noAvatar.png";
export default function Profile() {
  const { id } = useParams();
  const [didMount, setDidMount] = useState(false);
  const [profile, setProfile] = useState(null);
  const [posts, setPosts] = useState([]);
  const [saveTab, setSaveTab] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadPost, setLoadPost] = useState(false);
  const fetchProfileUser = async () => {
    setLoading(true);
    const user = await axiosClient.get(`/api/user/get/user?userId=${id}`);
    setProfile(user.data);
    setLoading(false);
  };
  const fetachPostOfUser = async () => {
    setLoadPost(true);
    const posts = await axiosClient.get(`/api/post/own/post?userId=${id}`);
    setPosts(posts.data);
    setLoadPost(false);
  };

  useEffect(() => {
    setDidMount(true);
    fetchProfileUser();
    fetachPostOfUser();
    return () => setDidMount(false);
  }, []);

  if (!didMount) {
    return null;
  }

  return (
    <>
      <div className={styles.profile}>
        {loading ? (
          <Spin />
        ) : (
          <div className={styles.container}>
            <Info profile={profile} />
            <div className={styles.profile_tab}>
              <button
                className={saveTab ? "" : styles.active}
                onClick={() => setSaveTab(false)}
              >
                Posts
              </button>
              <button
                className={saveTab ? styles.active : ""}
                onClick={() => setSaveTab(true)}
              >
                Saved
              </button>
            </div>
            {loadPost ? <Spin /> : saveTab ? " " : <PostThumb posts={posts} />}
          </div>
        )}
      </div>
    </>
  );
}
