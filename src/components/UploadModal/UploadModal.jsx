import { useState, useRef } from "react";
import "antd/dist/antd.css";
import { Input } from "antd";
import { useSelector } from "react-redux";
import { IoMdClose } from "react-icons/io";
import { FaRegFileVideo } from "react-icons/fa";
import { BiWinkSmile, BiImageAdd } from "react-icons/bi";
import { IoIosCheckmarkCircle } from "react-icons/io";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import { selectUser } from "../../features/authSlice";
import styles from "./UploadModal.module.scss";
import axiosClient from "../../axios/aixos";

const { TextArea } = Input;

function UploadModal({ onClose = () => {} }) {
  const [status, setStatus] = useState("");
  const [isOneImage, setIsOneImage] = useState(false);
  const [picture, setPicture] = useState(null);
  const [file, setFile] = useState(null);

  const PF = "http://localhost:8800/images/";
  const desc = useRef();
  const infoUser = useSelector(selectUser);
  const [pickerOpen, setPickerOpen] = useState(false);

  const onTypeStatus = (e) => {
    const value = e.target.value;
    setStatus(value);
  };
  const handleTogglePicker = () => {
    if (pickerOpen) {
      setPickerOpen(false);
    } else {
      setPickerOpen(true);
    }
  };

  const handleDisablePicker = () => {
    if (pickerOpen) {
      setPickerOpen(false);
    } else return;
  };

  const onClickIcon = (emoji) => {
    setStatus((prevContent) => (prevContent += emoji.native));
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    const newPost = {
      userId: infoUser._id,
      desc: status,
    };
    if (file) {
      const data = new FormData();
      const fileName = Date.now() + file.name;
      data.append("name", fileName);
      data.append("file", file);
      newPost.img = fileName;
      try {
        await axiosClient.post("/api/upload", data);
      } catch (err) {}
    }
    try {
      await axiosClient.post("/api/post/", newPost);
      setFile(null);
      setStatus("");
      setIsOneImage(false);
      window.location.reload();
    } catch (err) {}
  };

  const onChangePicture = (e) => {
    if (file) {
      alert("you just can post one img");
    }
    setPicture(URL.createObjectURL(e.target.files[0]));
    setFile(e.target.files[0]);
    if (e.target.files[0]) {
      setIsOneImage(true);
    }
  };

  return (
    <div className={styles.wrapperUpload}>
      <div className={styles.headerUpload}>
        Create post
        <div className={styles.btnClose} onClick={onClose}>
          <IoMdClose />
        </div>
      </div>
      <div className={styles.bodyUpload}>
        <div className={styles.inforUserPost}>
          <div className={styles.avtUser}>
            <div className={styles.avtImg}>
              <img
                src={
                  infoUser.profilePicture
                    ? PF + infoUser.profilePicture
                    : PF + "person/noAvatar.png"
                }
                alt=""
              />
            </div>
          </div>
          <div className={styles.userName}>
            <h3>{infoUser.userName}</h3>
            <span>
              <IoIosCheckmarkCircle className={styles.iconCheck} />
            </span>
          </div>
        </div>
        <div className={styles.inputStatus}>
          <TextArea
            className={styles.inputArea}
            value={status}
            onClick={handleDisablePicker}
            onChange={onTypeStatus}
            ref={desc}
            bordered={false}
            placeholder="Hi, What are you thinking about?"
            autoSize={{ minRows: 3, maxRows: 8 }}
          />
        </div>
        <div className={styles.emoji}>
          {pickerOpen && (
            <Picker
              showPreview="false"
              skin={2}
              defaultSkin={5}
              onClick={(emoji) => onClickIcon(emoji)}
              style={{
                position: "absolute",
                top: "-185px",
                right: "-350px",
              }}
            />
          )}
          <div className={styles.btnEmoji} onClick={handleTogglePicker}>
            <BiWinkSmile />
          </div>
        </div>
        {isOneImage && (
          <div className={styles.imgStatus}>
            <img src={picture} alt="" />
          </div>
        )}
        <div className={styles.chooseFormat}>
          <div className={styles.titleChoose}>ADD TO POST</div>
          <div className={styles.listChoose}>
            <div className={styles.chooseFormatItem}>
              <input
                type="file"
                id="uploadImg"
                multiple="multiple"
                hidden
                accept="image/png, image/jpeg"
                onChange={onChangePicture}
              />
              <label
                for="uploadImg"
                className={[styles.customImportFile, styles.importImage].join(
                  " "
                )}
              >
                <BiImageAdd className={styles.iconImage} /> Upload Image
              </label>
            </div>
            <div className={styles.chooseFormatItem}>
              <input
                type="file"
                multiple="multiple"
                disabled
                hidden
                id="uploadVideo"
                accept="video/mp4,video/x-m4v,video/*"
              />
              <label
                for="uploadVideo"
                className={[styles.customImportFile, styles.importVideo].join(
                  " "
                )}
              >
                <FaRegFileVideo className={styles.iconVideo} /> Upload Video
              </label>
            </div>
          </div>
        </div>
        <div className={styles.btnUpload}>
          <button onClick={submitHandler}>POST</button>
        </div>
      </div>
    </div>
  );
}
export default UploadModal;
