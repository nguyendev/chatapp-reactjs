export interface User {
  id: string;
  displayName?: string;
  avatarUrl?: string;
  email?: string;
  password?: string;
}
