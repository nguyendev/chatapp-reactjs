import "./App.css";
import {
  Switch,
  Route,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import Home from "./components/Home/Home";
import { useSelector, useDispatch } from "react-redux";
import { selectIsAuthenticated } from "./features/authSlice";
import { useEffect } from "react";
import { checkAuthState } from "./features/authSlice";
import "antd/dist/antd.css";
import PrivateRouter from "./customRouter/PrivateRouter";
import Navigation from "./components/Navigation/Navigation";
import Auth from "./controllers/Auth/Auth";
import Register from "./controllers/Register/Register";
import Profile from "./components/profile";
import PostDetail from "./components/PostDetail"

const token = JSON.parse(`${localStorage.getItem("user")}`)?.token;
function App() {
  useEffect(() => {
    dispatch(checkAuthState());
  }, []);
  const dispatch = useDispatch();

  const isAuthenticated = useSelector(selectIsAuthenticated);
  return (
    <div className="App">
      <Router>
        {isAuthenticated ? <Navigation></Navigation> : ""}
        <Switch>
          <Route
            exact
            path="/"
            component={isAuthenticated ? Home : Auth}
          />
          {!token ? <Redirect to="/" /> : ""}
          <Route exact path="/register" component={Register} />
          <PrivateRouter path="/profile/:id" component={Profile} />
          <PrivateRouter path="/post/:id" component={PostDetail}/>
        </Switch>

        {/* <Layout>{routes}</Layout> */}
      </Router>
    </div>
  );
}

export default App;
