import axios from "axios";
import queryString from "querystring";

const axiosClient = axios.create({
  baseURL: "http://localhost:8800",
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

axiosClient.interceptors.request.use((req) => {
  const token = JSON.parse(localStorage.getItem("user")).token;
  req.headers.Authorization = token ? `Bearer ${token}` : "";
  return req;
});

axiosClient.interceptors.response.use(
  (response) => response,
  (error) => {
    throw error;
  },
);

export default axiosClient;