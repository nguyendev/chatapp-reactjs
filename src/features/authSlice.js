import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  token: null,
  isLoading: false,
  error: null,
  userName: null,
  followers: [],
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    authInit: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    authSuccess: (state, { payload }) => {
      state.isLoading = false;
      state.token = payload[0];
      state.userName = payload[1];
      state.followers = payload[2];
      state.error = null;
    },
    authFail: (state, { payload }) => {
      state.isLoading = false;
      state.error = payload;
    },
    authLogout: (state) => {
      localStorage.removeItem("user");
      localStorage.removeItem("expirationTime");
      localStorage.removeItem("firstLogin");
      state.token = null;
    },
  },
});

export const { authInit, authSuccess, authFail, authLogout } =
  authSlice.actions;

const setTimeOutForToken = (expiresIn) => (dispatch) => {
  setTimeout(() => {
    dispatch(authLogout());
  }, expiresIn * 1000);
};

export const asyncSignIn = (formData) => async (dispatch) => {
  dispatch(authInit());
  try {
    formData = { ...formData, returnSecureToken: true };
    const result = await axios.post(
      `http://localhost:8800/api/auth/signin`,
      formData
    );

    localStorage.setItem(
      "user",
      JSON.stringify({
        token: result.data.token,
        _id: result.data.user._id,
        email: result.data.user.email,
        avatar: result.data.user.profilePicture,
        userName: result.data.user.userName,
        followers: result.data.user.followers,
        saved: result.data.user.saved,
      })
    );
    localStorage.setItem("firstLogin", true);
    dispatch(
      authSuccess([
        result.data.token,
        result.data.user.userName,
        result.data.user.followers,
      ])
    );
  } catch (error) {
    console.log(error);
    // dispatch(authFail(error.response.data.error.message));
  }
};

export const checkAuthState = () => (dispatch) => {
  const token = JSON.parse(localStorage.getItem("user"))?.token;
  if (!token) {
    dispatch(authLogout());
  } else {
    dispatch(authSuccess(token));
    // const expirationTime = new Date(localStorage.getItem("expirationTime"));
    // if (expirationTime <= new Date()) {
    //   dispatch(authLogout());
    // } else {

    //   dispatch(
    //     setTimeOutForToken(
    //       (expirationTime.getTime() - new Date().getTime()) / 1000
    //     )
    //   );
    // }
  }
};
export const selectIsLoading = (state) => state.auth.isLoading;
export const selectError = (state) => state.auth.error;
export const selectUser = (state) => {
  return {
    _id: JSON.parse(localStorage.getItem("user"))?._id,
    userName: JSON.parse(localStorage.getItem("user"))?.userName,
    token: JSON.parse(localStorage.getItem("user"))?.token,
    profilePicture: JSON.parse(localStorage.getItem("user"))?.avatar,
    followers: JSON.parse(localStorage.getItem("user"))?.followers,
    saved: JSON.parse(localStorage.getItem("user"))?.saved,
  };
};
export const selectIsAuthenticated = (state) => !!state.auth.token;
export default authSlice.reducer;
