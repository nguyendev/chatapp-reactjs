import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  isLoading: false,
  statusRegister: false,
  msg: null,
};

const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    registerInit: (state) => {
      state.isLoading = true;
      state.msg = null;
    },
    registerSuccess: (state, { payload }) => {
      state.isLoading = false;
      state.statusRegister = true;
      state.msg = payload;
    },
    registerFail: (state, { payload }) => {
      state.isLoading = false;
      state.statusRegister = false;
      state.msg = payload;
    },
  },
});

export const { registerInit, registerFail, registerSuccess } =
  registerSlice.actions;

export const asyncRegister = (formData) => async (dispatch) => {
  dispatch(registerInit());
  formData = { ...formData, returnSecureToken: true };
  const result = await axios.post(
    `http://localhost:8800/api/auth/register`,
    formData
  );

  if (result.status === 200) {
    dispatch(registerSuccess(result.status));
  } else {
    dispatch(registerFail(result.status));
  }
};

export const selectIsLoading = (state) => state.register.isLoading;
export const selectError = (state) => state.register.error;
export const selectMsg = (state) => state.register.msg;
export const statusRegister = (state) => state.register.statusRegister;
export default registerSlice.reducer;
