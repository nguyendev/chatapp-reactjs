import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useDispatch } from "react-redux";
import { authLogout } from "../../../features/authSlice";
export default function Logout() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(authLogout());
  }, []);
  return <Redirect to="/" />;
}
