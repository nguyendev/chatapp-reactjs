import { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { ImFacebook2 } from "react-icons/im";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import {
  selectIsLoading,
  asyncSignIn,
  selectIsAuthenticated,
} from "../../features/authSlice";
import styles from "./Auth.module.scss";
import Insta from "../../assets/img/Insta.png";

export default function Auth() {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const isAuthenticated = useSelector(selectIsAuthenticated);
  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [isAllow, setIsAllow] = useState(false);
  const handleOnchange = (e) => {
    const tempData = { ...data };
    tempData[e.target.name] = e.target.value;
    let isAllow = true;
    for (const item in tempData) {
      isAllow = !!tempData[item] && isAllow;
    }
    setData(tempData);
    setIsAllow(isAllow);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isAllow) {
      dispatch(asyncSignIn(data));
    }
  };
  let form = (
    <div className="wrapperAuth">
      <div className={styles.containerAuth}>
        <div className={styles.imgInsta}>
          <img src={Insta} alt="" />
        </div>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input username!" }]}
          >
            <Input
              value={data.email}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Phone number, username or email"
              onChange={handleOnchange}
              name="email"
              type="email"
              className={styles.loginInput}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              value={data.password}
              placeholder="Password"
              name="password"
              className={styles.loginInput}
              onChange={handleOnchange}
            />
          </Form.Item>
          <Form.Item>
            <Button
              onClick={handleSubmit}
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              LOG IN
            </Button>
          </Form.Item>
        </Form>
        <div className={styles.lineSuggest}></div>
        <div className={styles.loginFace}>
          <ImFacebook2 className={styles.iconFace} />
          <p>Log in with Facebook</p>
        </div>
        <Link to="" className={styles.forgotPass}>
          Forgot password?
        </Link>
      </div>
      <div className={styles.switchSignUp}>
        <p>Don't have an account?</p> <Link to="/register">Sign up</Link>
      </div>
    </div>
  );
  if (isLoading) {
    form = <Spinner />;
  }
  if (isAuthenticated) {
    return <Redirect to={`/`} />;
  }
  return (
    <div className={styles.AuthForm}>
      {form}
    </div>
  );
}
