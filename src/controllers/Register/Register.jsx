import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Input, Button, notification } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import {
  selectIsLoading,
  asyncRegister,
  statusRegister,
  selectMsg,
} from "../../features/registerSlice";
import styles from "./Register.module.scss";
import { Link } from "react-router-dom";
import { ImFacebook2 } from "react-icons/im";
import Insta from "../../assets/img/Insta.png";

export default function Register() {
  const dispatch = useDispatch();
  const [api, contextHolder] = notification.useNotification();
  const isLoading = useSelector(selectIsLoading);
  const status = useSelector(statusRegister);
  const msgRes = useSelector(selectMsg);
  const [data, setData] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [isAllow, setIsAllow] = useState(false);

  const handleOnchange = (e) => {
    const tempData = { ...data };
    tempData[e.target.name] = e.target.value;
    let isAllow = true;
    for (const item in tempData) {
      isAllow =
        !!tempData[item] &&
        isAllow &&
        tempData.password === tempData.confirmPassword;
    }
    setData(tempData);
    setIsAllow(isAllow);
  };

  const openNotification = (placement, status) => {
    api.info({
      message: `Notification ${placement}`,
      description: status ? "Register Successfully!" : "Register Failure",
      placement,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (isAllow) {
      dispatch(asyncRegister(data));
      console.log(msgRes);
      openNotification("bottomRight", status);
    }
  };

  let form = (
    <div className="wrapperAuth">
      <div className={styles.containerAuth}>
        <div className={styles.imgInsta}>
          <img src={Insta} alt="" />
        </div>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Please input username!" }]}
          >
            <Input
              value={data.email}
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Phone number, username or email"
              onChange={handleOnchange}
              name="email"
              type="email"
              className={styles.loginInput}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              value={data.password}
              placeholder="Password"
              name="password"
              className={styles.loginInput}
              onChange={handleOnchange}
            />
          </Form.Item>
          <Form.Item
            name="confirmPassword"
            rules={[
              {
                required: true,
                message: "Please input your confirm password!",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              value={data.confirmPassword}
              placeholder="Confirm Password"
              name="confirmPassword"
              className={styles.loginInput}
              onChange={handleOnchange}
            />
          </Form.Item>
          <Form.Item>
            <Button
              onClick={handleSubmit}
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Register
            </Button>
          </Form.Item>
        </Form>
        <div className={styles.lineSuggest}></div>
        <div className={styles.loginFace}>
          <ImFacebook2 className={styles.iconFace} />
          <p>Log in with Facebook</p>
        </div>
      </div>
      <div className={styles.switchSignUp}>
        <p>I have already an account</p> <Link to="/">Sign in!</Link>
      </div>
    </div>
  );
  return (
    <>
      {contextHolder}
      <div className={styles.AuthForm}>{form}</div>
    </>
  );
}
